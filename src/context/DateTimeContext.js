import React from "react";

const DateTimeContext = React.createContext();
const DateTimeProvider = DateTimeContext.Provider;

export { DateTimeProvider }
export default DateTimeContext