import React, { Component, PureComponent } from 'react'
import RegularChildComponent from './RegularChildComponent'
import RegularfunctionComponent from './RegularfunctionComponent';

export class RegularComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            name: 'Tamil'
        }
    }

    componentDidMount() {
        setInterval(() => {
            this.setState({
                name: 'Tamil'
            })
        }, 1000);
    }

    render() {
        console.log("Regular Component Rerendered");
        return (
            <div>
                {this.state.name}
                <RegularfunctionComponent name={this.state.name} />
            </div>
        )
    }
}

export default RegularComponent
