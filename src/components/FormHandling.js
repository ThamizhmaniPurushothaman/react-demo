import React, { Component } from 'react'

export class FormHandling extends Component {
    constructor(props) {
        super(props)

        this.state = {
            username: '',
            email: '',
            password: ''
        }
    }
    fnSetUserName = (event) => {
        this.setState({
            username: event.target.value
        })
    }
    fnSetEmail = (event) => {
        this.setState({
            email: event.target.value
        })
    }

    fnSetPassword = (event) => {
        this.setState({
            password: event.target.value
        })
    }

    fnSubmit = (event) => {
        event.preventDefault();
        console.log(this.state);
    }


    render() {
        return (
            <div>
                <form onSubmit={this.fnSubmit}>
                    <div>
                        <label>UserName</label>
                        <input type="text" value={this.state.username} onChange={this.fnSetUserName} />
                    </div>
                    <div>
                        <label>Email</label>
                        <input type="email" value={this.state.email} onChange={this.fnSetEmail} />
                    </div>
                    <div>
                        <label>Password</label>
                        <input type="password" value={this.state.password} onChange={this.fnSetPassword} />
                    </div>
                    <div>
                        <button type="submit">Register</button>
                    </div>
                </form>
            </div>
        )
    }
}

export default FormHandling
