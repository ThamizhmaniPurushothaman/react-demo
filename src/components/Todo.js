import React, { useState } from 'react'

const Todo = ({ todo, index, dispatch }) => {
    const [isDone, setDone] = useState(false);

    const todoStyle = {
        'color': (todo.isDone ? 'green' : ''),
        'text-decoration-line': (todo.isDone ? 'line-through' : '')
    }
    return <div key={index}>
        <div >
            <div style={todoStyle}> {index + 1} . {todo.itemName} </div>
            <button onClick={() => dispatch({ type: 'REMOVE', index: index })}>Delete</button>
            <input type="checkbox" value={isDone} onChange={() => dispatch({ type: 'DONE', index: index })} />
        </div>
    </div>
}

export default Todo