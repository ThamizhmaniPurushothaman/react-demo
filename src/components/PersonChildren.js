import React, { Component } from 'react'

export class PersonChildren extends Component {
    render() {
        console.log(this.props);
        return (
            <div>
                Person Children {this.props.person.name}{this.props.person.age}
            </div>
        )
    }
}

export default PersonChildren
