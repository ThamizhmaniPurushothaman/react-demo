import React, { Component } from 'react'
import { LifeCycleB } from './LifeCycleB';

export class LifeCycleA extends Component {

    constructor(props) {
        super(props)
        console.log("LifeCycleA constructor Method");
        this.state = {
            message: ''
        }
    }

    static getDerivedStateFromProps() {
        console.log("LifeCycleA getDerivedStateFromProps Method");
        return null;
    }

    componentDidMount() {
        console.log("LifeCycleA componentDidMount Method");
    }
    shouldComponentUpdate(currentState, prevState) {
        console.log("LifeCycleA shouldComponentUpdate Method");
        if (prevState.message == this.state.message) { return false; }
        return true;
    }

    componentDidUpdate() {
        console.log("LifeCycleA componentDidUpdate Method");
    }


    fnChangeMessage = () => {
        this.setState({
            message: 'Hi'
        })
    }

    render() {
        console.log("LifeCycleA render Method");
        return (
            <div>
                {this.state.message}
                <button type='button' onClick={this.fnChangeMessage}>Change Message</button>
                <LifeCycleB />
            </div>
        )
    }
}

export default LifeCycleA
