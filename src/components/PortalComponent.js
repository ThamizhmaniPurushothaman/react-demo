import React, { Component } from 'react'
import ReactDOM from 'react-dom';


export function PortalAppComponent() {
    return ReactDOM.createPortal((
        <div>
            Portal App Component
        </div>
    ), document.getElementById('portal-root'))
}

export default PortalAppComponent
