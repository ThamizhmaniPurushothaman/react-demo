import React from 'react'

const PersonFunction = (props) => {
    const { name, age } = props.person;
    return (
        <div>
            My First Functional Component {name} {age}
        </div>
    )
}

export default PersonFunction
