import React, { Component } from 'react'
import ListRendereingChild from './ListRendereingChild'

export class ListRendering extends Component {
    constructor(props) {
        super(props)

        this.state = {
            persons: [
                { Id: 1, name: 'Tamil', age: '29' },
                { Id: 2, name: 'Ram', age: '23' },
                { Id: 3, name: 'Sam', age: '27' },
                { Id: 4, name: 'Karthik', age: '29' },
            ]
        }
    }

    render() {
        return (
            <div>
                <ul>
                    {
                        this.state.persons.map((person) => {
                            return <ListRendereingChild key={person.Id} personProp={person} />
                        })
                    }
                </ul>

            </div>
        )
    }
}

export default ListRendering
