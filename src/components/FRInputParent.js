import React, { Component } from 'react'
import FRInputChild from './FRInputChild';

export class FRInputParent extends Component {
    constructor(props) {
        super(props)
        this.refChildComponentRef = React.createRef();

    }
    fnFocusComponentInput = () => {
        this.refChildComponentRef.current.focus();
    }

    render() {
        return (
            <div>
                <FRInputChild ref={this.refChildComponentRef} />
                <button onClick={this.fnFocusComponentInput}>Focus Input</button>
            </div>
        )
    }
}

export default FRInputParent
