import React, { Component } from 'react'
import MethodsAsPropChild from './MethodsAsPropChild';

export class MethodAsPropParent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            message: 'Hi Guest'
        }
    }

    fnChangeMessage = () => {
        this.setState({
            message: 'Hi Ram'
        });
    }

    render() {
        return (
            <div>
                <div>{this.state.message}</div>
                <MethodsAsPropChild ChangeMessageEventHandler={this.fnChangeMessage} />
            </div>
        )
    }
}

export default MethodAsPropParent
