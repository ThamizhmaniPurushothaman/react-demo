import React from 'react'

function RegularfunctionComponent(props) {
    console.log("Regular Function Called");
    return (
        <div>
            {props.name}
        </div>
    )
}

export default React.memo(RegularfunctionComponent)
