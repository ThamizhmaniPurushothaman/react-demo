import React from 'react'

function ContextUsage4(props) {
    return (
        <div>
            {props.user}{props.theme}
        </div>
    )
}

export default ContextUsage4
