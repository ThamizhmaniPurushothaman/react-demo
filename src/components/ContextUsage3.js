import React, { Component } from 'react'
import UserContext from '../context/UserContext'

export class ContextUsage3 extends Component {
    render() {
        return (
            <div>
                <div>{this.context}</div>
            </div>
        )
    }
}
ContextUsage3.contextType = UserContext;

export default ContextUsage3
