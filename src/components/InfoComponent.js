import React, { Component } from 'react'

export class InfoComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {

        }
    }

    componentDidMount() {
        if (this.props.information == '') {
            throw new Error("Invalid Information");
        }
    }

    render() {
        return (
            <div>
                {this.props.information}
            </div>
        )
    }
}

export default InfoComponent
