import React, { Component } from 'react'

export class ConditionalRendering extends Component {
    constructor(props) {
        super(props)

        this.state = {
            isLoggedIn: true
        }
    }

    render() {
        const renderElement = !this.state.isLoggedIn ? <div> Welcome Guest </div> : <div> Welcome User </div>;
        const renderElement1 = this.state.isLoggedIn && <div> Welcome User </div>;
        // if (!this.state.isLoggedIn) {
        //     return (
        //         <div>
        //             Welcome Guest
        //             <span>Fita Institite</span>
        //         </div>
        //     )
        // }
        // else {
        //     return (
        //         <div>
        //             Welcome User
        //             <span>Fita Institite</span>
        //         </div>
        //     )
        // }

        // return (
        //     <div>
        //         {
        //             !this.state.isLoggedIn ? <div> Welcome Guest </div> : <div> Welcome User </div>
        //         }
        //         <span>Fita Institite</span>
        //     </div>
        // )



        return (
            <div>
                {renderElement1}
                <span>Fita Institite</span>
            </div>
        )

    }
}

export default ConditionalRendering
