import React, { Component } from "react";
import PersonChildren from "./PersonChildren";

class Person extends Component {

    render() {
        const { name, age } = this.props.person;
        return <div>
            My First Class Component {name}{age}
            <PersonChildren person={this.props.person} />
        </div>
    }

}
export default Person