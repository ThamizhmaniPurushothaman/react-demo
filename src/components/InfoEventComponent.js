import React, { Component } from 'react'
import ErrorBoundaries from './ErrorBoundaries'
import InfoComponent from './InfoComponent'

export class InfoEventComponent extends Component {
    render() {
        return (
            <div>
                <ErrorBoundaries>
                    <InfoComponent information="Saved Success" />
                </ErrorBoundaries>
                <ErrorBoundaries>
                    <InfoComponent information="Update Success" />
                </ErrorBoundaries>
                <ErrorBoundaries>
                    <InfoComponent information="" />
                </ErrorBoundaries>
            </div>
        )
    }
}

export default InfoEventComponent
