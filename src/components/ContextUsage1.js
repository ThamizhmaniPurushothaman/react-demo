import React, { Component } from 'react'
import ContextUsage2 from './ContextUsage2'
import ContextUsage3 from './ContextUsage3'

export class ContextUsage1 extends Component {
    render() {
        return (
            <div>
                <ContextUsage2 />
                <ContextUsage3 />
            </div>
        )
    }
}

export default ContextUsage1
