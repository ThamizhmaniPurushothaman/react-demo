import React, { Component } from 'react'
import ThemeContext from '../context/ThemeContext'
import { UserConsumer } from '../context/UserContext'
import ContextUsage4 from './ContextUsage4'

export class ContextUsage2 extends Component {
    render() {
        return (
            <div>
                <UserConsumer>
                    {
                        (userName) => (
                            <ThemeContext.Consumer>
                                {
                                    (theme) => (
                                        <ContextUsage4 user={userName} theme={theme} />
                                    )
                                }
                            </ThemeContext.Consumer>
                        )
                    }
                </UserConsumer>
            </div>
        )
    }
}

export default ContextUsage2
