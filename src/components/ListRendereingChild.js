import React from 'react'

function ListRendereingChild(props) {
    const { Id, name, age } = props.personProp;
    return (
        <li>{name} {age}</li>
    )
}

export default ListRendereingChild
