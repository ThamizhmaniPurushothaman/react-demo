import React, { Component } from 'react'
import FragementChildComponent from './FragementChildComponent'

export class FragmentComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            students: [
                { name: 'Tamil', age: '29' },
                { name: 'Ram', age: '30' },
            ]
        }
    }

    render() {
        return (
            <div>
                <table>
                    <thead>
                        <tr>
                            <th>
                                Name
                            </th>
                            <th>
                                Age
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <FragementChildComponent students={this.state.students} />
                    </tbody>
                </table>
            </div>
        )
    }
}

export default FragmentComponent
