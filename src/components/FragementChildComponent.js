import React from 'react'

function FragementChildComponent(props) {
    return (
        <React.Fragment>
            {
                props.students.map((student, index) => {
                    return <tr key={index}>
                        <td>{student.name}</td>
                        <td>{student.age}</td>
                    </tr>
                })
            }
        </React.Fragment>
    )
}

export default FragementChildComponent
