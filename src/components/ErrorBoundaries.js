import React, { Component } from 'react'

export class ErrorBoundaries extends Component {
    constructor(props) {
        super(props)

        this.state = {
            hasError: false
        }
    }

    static getDerivedStateFromError() {
        return {
            hasError: true
        }
    }
    // componentDidCatch() {
    //     this.setState({ hasError: true });
    // }


    render() {
        return (
            <div>
                {
                    this.state.hasError ? <div>404 Not Found</div> : this.props.children
                }
            </div>
        )
    }
}

export default ErrorBoundaries
