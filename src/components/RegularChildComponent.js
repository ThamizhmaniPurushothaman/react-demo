import React from 'react'

function RegularChildComponent(props) {
    console.log("Regular Child Component Rerendered");
    return (
        <div>
            {props.name}
        </div>
    )
}

export default RegularChildComponent
