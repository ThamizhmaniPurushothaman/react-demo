import React from 'react'

function MethodsAsPropChild(props) {
    return (
        <div>
            <button onClick={props.ChangeMessageEventHandler}>Change</button>
        </div>
    )
}

export default MethodsAsPropChild
