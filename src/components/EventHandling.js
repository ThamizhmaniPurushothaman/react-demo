import React, { Component } from 'react'

export class EventHandling extends Component {
    constructor(props) {
        super(props)

        this.state = {
            message: 'Welcome To React'
        }
        console.log("Constructor");
        this.fnChangeMessage3 = this.fnChangeMessage3.bind(this);
    }

    fnChangeMessage1() {
        console.log(this);
        console.log("Working");

    }
    fnChangeMessage2() {
        console.log(this);
        this.setState({
            message: 'Subscribed'
        })
    }
    fnChangeMessage3() {
        this.setState({
            message: 'Subscribed'
        })
    }
    fnChangeMessage4() {
        this.setState({
            message: 'Subscribed'
        })
    }
    fnChangeMessage5 = () => {
        this.setState({
            message: 'Subscribed'
        })
    }

    render() {
        console.log("Rendered");
        return (
            <div>
                <span>{this.state.message}</span>
                <div>
                    1. <button onClick={this.fnChangeMessage1}>No this usage</button>
                </div>
                <div>
                    2. <button onClick={this.fnChangeMessage2.bind(this)}>this binding in click event</button>
                </div>
                <div>
                    3. <button onClick={this.fnChangeMessage3} > this binding in constructor</button>
                </div>
                <div>Use Methods Like Below</div>
                <div>
                    4. <button onClick={() => this.fnChangeMessage4()} > Using Callback Function</button>
                </div>
                <div>
                    5. <button onClick={this.fnChangeMessage5} >Usig Arrow Funtion</button>
                </div>
            </div>
        )
    }
}

export default EventHandling
