import React, { Component } from 'react'


export class StyleSheetComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            NeedStyling: false
        }
    }


    render() {
        const primaryText = this.state.NeedStyling ? 'text-primary' : '';

        const inlineStyleObj = {
            backgroundColor: this.state.NeedStyling ? 'red' : 'green',
            color: 'white',
        }

        return (
            <div>
                {/* <h2 className={`${primaryText} font-xl`}>Using Class Styling</h2>

                <h2 style={inlineStyleObj}>Inline Styling</h2> */}

                {/* <h2 className={styles.Primary}>Modules Css Styling</h2> */}
            </div>
        )
    }
}

export default StyleSheetComponent
