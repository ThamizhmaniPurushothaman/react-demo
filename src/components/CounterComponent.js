import React, { Component } from 'react'
import CouterChildComponent from './CouterChildComponent';

export class CounterComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            count: 0
        }
    }

    fnIncrement() {
        // this.setState({
        //     count: this.state.count + 1
        // }, () => {
        //     console.log(this.state.count);
        // });

        this.setState((prevState) => {
            return {
                count: prevState.count + 1
            };
        });

    }
    fnIncrementBy5() {
        this.fnIncrement();
        this.fnIncrement();
        this.fnIncrement();
        this.fnIncrement();
        this.fnIncrement();
    }

    render() {
        return (
            <div>
                Counter Component
                <CouterChildComponent count={this.state.count} />
                <button onClick={() => this.fnIncrement()}>Increment By 1</button>
                <button onClick={() => this.fnIncrementBy5()}>Increment By 5</button>
            </div>
        )
    }
}

export default CounterComponent
