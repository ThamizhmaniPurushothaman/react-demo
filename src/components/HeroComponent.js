import React, { Component } from 'react'

class HeroComponent extends Component {
    constructor() {
        super()

        this.state = {
            message: 'Hi Guest',
            age: 20
        };
    }

    fnChangeMessage() {
        this.setState({
            message: 'Hi Ram'
        });
    }

    render() {
        return (
            <div>
                Hero Component
                <div> {this.state.message} {this.state.age}</div>
                <button onClick={() => this.fnChangeMessage()}>Change Message</button>
            </div>
        )
    }
}

export default HeroComponent

