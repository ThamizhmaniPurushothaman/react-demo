import React, { Component } from 'react'

export class RefComponents extends Component {
    constructor(props) {
        super(props)
        this.usernameInputRef = React.createRef();

        this.cbUserNameInputRef = null;

        this.cbUserNameInputSetRef = (element) => {
            this.cbUserNameInputRef = element
        }

    }


    fnFocusInput = () => {
        console.log(this.cbUserNameInputRef);
        //this.usernameInputRef.current.focus();
        this.cbUserNameInputRef.focus();
    }

    render() {
        return (
            <div>
                <input type="text" ref={this.cbUserNameInputSetRef} />
                <button onClick={this.fnFocusInput}>Focus Input</button>
            </div>
        )
    }
}
export default RefComponents
