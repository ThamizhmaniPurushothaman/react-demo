import React, { Component } from 'react'

export class LifeCycleB extends Component {

    constructor(props) {
        super(props)
        console.log("LifeCycleB constructor Method");
        this.state = {
            message: ''
        }
    }

    static getDerivedStateFromProps() {
        console.log("LifeCycleB getDerivedStateFromProps Method");
        return null;
    }

    componentDidMount() {
        console.log("LifeCycleB componentDidMount Method");
    }
    shouldComponentUpdate(currentState, prevState) {
        console.log("LifeCycleB shouldComponentUpdate Method");
        return true;
    }

    componentDidUpdate() {
        console.log("LifeCycleB componentDidUpdate Method");
    }



    render() {
        console.log("LifeCycleB render Method");
        return (
            <div>

            </div>
        )
    }
}

export default LifeCycleB
