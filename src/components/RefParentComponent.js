import React, { Component } from 'react'
import RefChildComponent from './RefChildComponent'

export class RefParentComponent extends Component {
    constructor(props) {
        super(props)
        this.refChildComponentRef = React.createRef();

    }
    fnFocusComponentInput = () => {
        this.refChildComponentRef.current.focusInput();
    }

    render() {
        return (
            <div>
                <RefChildComponent ref={this.refChildComponentRef} />
                <button onClick={this.fnFocusComponentInput}>Focus Input</button>
            </div>
        )
    }
}

export default RefParentComponent
