
const fnTodoReducer = (todos, action) => {
    switch (action.type) {
        case 'ADD':
            return [...todos, action.todoItem];
            break;
        case 'REMOVE':
            return todos.filter((item, index) => index != action.index);
            break;
        case 'DONE':
            return todos.map((item, index) => {
                if (index == action.index) {
                    item.isDone = !item.isDone;
                }
                return item;
            });
            break;
        default:
            break;
    }
}
export default fnTodoReducer