import React, { useState, useReducer } from 'react';

const CountReducer = (state, action) => {
    switch (action) {
        case 'Increment':
            return state + 1;
            break;
        case "Decrement":
            return state - 1;
            break;
        case 'Multiply':
            return state * 5;
            break;
        case 'Divide':
            return state / 2;
            break;
        default:
            break;
    }
}

const CountReducer2 = (state) => {
    return "ChangedText"
}

const UseReducer1 = () => {
    const [count, dispatch] = useReducer(CountReducer, 0);
    const [TextName, Textdispatch] = useReducer(CountReducer2, "Fita Academy");

    return <div>
        <h2>{count}</h2>
        <h2>{TextName}</h2>
        <button onClick={() => dispatch('Increment')}>Increment</button>
        <button onClick={() => dispatch('Decrement')}>Decrement</button>
        <button onClick={() => dispatch('Multiply')}>Multiply</button>
        <button onClick={() => dispatch('Divide')}>Divide</button>
        <button onClick={() => Textdispatch('ChangeText')}>ChangeText</button>
    </div>;
};


export default UseReducer1;
