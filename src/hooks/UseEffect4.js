import React, { useEffect, useState } from 'react'
import axios from 'axios'

const UseEffect4 = () => {

    const [posts, setPost] = useState([]);

    useEffect(() => {
        // axios({
        //     method: 'get',
        //     url: 'https://jsonplaceholder.typicode.com/posts',
        // })
        //     .then(function (response) {
        //         console.log(response.data);
        //         setPost(response.data);
        //     });
        fetch('https://jsonplaceholder.typicode.com/posts')
            .then(response => response.json())
            .then(jsonResponse => setPost(jsonResponse))
    }, []);

    return <div>
        {
            posts.map((post) => {
                return <h2>{post.title}</h2>
            })
        }
    </div>;
};

export default UseEffect4;
