import React, { useContext } from 'react';
import DateTimeContext from '../context/DateTimeContext';
import UserContext from '../context/UserContext';

const UseContext1 = () => {
    const value = useContext(DateTimeContext);
    const userName = useContext(UserContext);
    return <div>{value}{userName}</div>;
};

export default UseContext1;
