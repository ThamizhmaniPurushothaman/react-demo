import React, { useReducer, useState } from 'react'
import Todo from '../components/Todo';

import fnTodoReducer from '../reducers/ToDoReducer';

const ToDoFunction = () => {

    const [todos, dispatch] = useReducer(fnTodoReducer, []);
    const [item, setItem] = useState('');


    const fnAddTodoItem = () => {
        dispatch({
            type: 'ADD',
            todoItem: {
                itemName: item,
                isDone: false
            }
        });
        setItem('');
    }

    return (
        <div>
            <input type="text" value={item} onChange={(e) => setItem(e.target.value)} />
            <button type="button" onClick={fnAddTodoItem}>Add</button>

            {
                todos.map((todo, index) => {
                    return <Todo todo={todo} index={index} dispatch={dispatch} />
                })
            }
        </div>
    )
}

export default ToDoFunction