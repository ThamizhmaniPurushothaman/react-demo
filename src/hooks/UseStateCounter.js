import React, { useState } from 'react'

function UseStateCounter() {
    const [count, setCount] = useState(0);
    const [user, setUser] = useState({ name: 're', age: '45' });
    const [theme, setTheme] = useState('dark');
    const [items, setItems] = useState([]);

    const fnSetCount = () => {
        setCount((prevCount) => prevCount + 1);
        setCount((prevCount) => prevCount + 1);
        setCount((prevCount) => prevCount + 1);
        // setUser({
        //     name: 'Tamil',
        //     age: 24
        // });

        setUser(() => {
            return {
                name: 'Tamil',
                age: 24
            };
        })
        setTheme('light');
    }
    const fnAddItems = () => {
        setItems(() => {
            return [...items, Math.random()];
        })
    }

    return (
        <div>
            {user.name}{user.age} {theme}
            {
                items.map((item) => {
                    return <div>{item}</div>
                })
            }
            <button type="button" onClick={fnSetCount}>Click {count}</button>
            <button type="button" onClick={fnAddItems}>Add Item</button>
        </div>
    )
}

export default UseStateCounter
