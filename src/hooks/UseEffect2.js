import React, { useState, useEffect } from 'react'

const UseEffect2 = () => {

    const [x, setX] = useState(0);
    const [y, setY] = useState(0);

    const logmousemovecoordinates = (event) => {
        setX(event.clientX);
        setY(event.clientY);
    }

    useEffect(() => {
        console.log("Use Effect Called Once");
        window.addEventListener("mousemove", logmousemovecoordinates)
        return () => {
            window.removeEventListener("mousemove", logmousemovecoordinates)
        }
    }, []);

    return (
        <div>
            X: {x}, Y: {y}
        </div>
    )
}

export default UseEffect2
