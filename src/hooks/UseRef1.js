import React, { useRef, useEffect } from 'react';

const UseRef1 = () => {

    const inputRef = useRef();
    useEffect(() => {
        inputRef.current.focus();
    }, []);

    return (
        <div>
            <input type="text" ref={inputRef} />
        </div>
    );
};

export default UseRef1;
