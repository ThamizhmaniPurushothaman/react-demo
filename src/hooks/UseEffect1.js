import React, { useState, useEffect } from 'react'

const UseEffect1 = () => {

    const [count, setCount] = useState(0);
    const [theme, setTheme] = useState('dark');

    useEffect(() => {
        console.log("Use Effect Called Always");
    });

    useEffect(() => {
        console.log("Use Effect Called Once");
    }, []);

    useEffect(() => {
        console.log("Use Effect Called only on theme");
    }, [theme]);

    const fnSetTheme = () => {
        if (theme == 'dark') {
            setTheme('light')
        }
        else {
            setTheme('dark')
        }
    }


    return (
        <div>
            <button onClick={() => setCount(count + 1)}>Clicked {count} times</button>
            <button onClick={fnSetTheme}>Set Theme</button>
        </div>
    )
}

export default UseEffect1
