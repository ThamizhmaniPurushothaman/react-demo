import Person from "./components/person";
import PersonFunction from "./components/person-function";
import './App.css'
import HeroComponent from "./components/HeroComponent";
import CounterComponent from "./components/CounterComponent";
import EventHandling from "./components/EventHandling";
import MethodAsPropParent from "./components/MethodAsPropParent";
import ListRendering from "./components/ListRendering";
import ConditionalRendering from "./components/ConditionalRendering";
import FormHandling from "./components/FormHandling";
import StyleSheetComponent from "./components/StyleSheetComponent";

import LifeCycleA from "./components/LifeCycleA";
import FragmentComponent from "./components/FragmentComponent";
import RegularComponent from "./components/RegularComponent";
import RegularfunctionComponent from "./components/RegularfunctionComponent";
import RefComponents from "./components/RefComponents";
import RefParentComponent from "./components/RefParentComponent";
import FRInputParent from "./components/FRInputParent";
import InfoEventComponent from "./components/InfoEventComponent";
import ContextUsage1 from "./components/ContextUsage1";
import { UserProvider } from "./context/UserContext";
import ThemeContext from "./context/ThemeContext";
import UseStateCounter from "./hooks/UseStateCounter";
import UseEffect1 from "./hooks/UseEffect1";
import UseEffect2 from "./hooks/UseEffect2";
import UseEffect3 from "./hooks/UseEffect3";
import UseEffect4 from "./hooks/UseEffect4";
import UseContext1 from "./hooks/UseContext1";
import { DateTimeProvider } from "./context/DateTimeContext";
import UseRef1 from "./hooks/UseRef1";
import UseReducer1 from "./hooks/UseReducer1";
import ToDoFunction from "./hooks/UseReducer2";


function App() {
  let personObj = {
    name: 'Ram',
    age: 25
  };
  return (
    <div className="centered">
      {/* <Person person={personObj} />
      <PersonFunction person={personObj} /> */}
      {/* <HeroComponent /> */}
      {/* <CounterComponent /> */}
      {/* <EventHandling /> */}
      {/* <MethodAsPropParent /> */}
      {/* <ListRendering /> */}
      {/* <ConditionalRendering /> */}
      {/* <FormHandling /> */}
      {/* <StyleSheetComponent /> */}
      {/* <div className={styles.Primary}>Demo</div> */}
      {/* <LifeCycleA /> */}
      {/* <FragmentComponent /> */}
      {/* <RegularComponent /> */}
      {/* <RefComponents /> */}
      {/* <RefParentComponent /> */}
      {/* <FRInputParent /> */}
      {/* <InfoEventComponent /> */}
      {/* <UserProvider value="Tamil">
        <ThemeContext.Provider value="dark">
          <ContextUsage1 />
        </ThemeContext.Provider>
      </UserProvider> */}
      {/* <UseStateCounter /> */}
      {/* <UseEffect1 /> */}
      {/* <UseEffect2 /> */}
      {/* <UseEffect3 /> */}
      {/* <UseEffect4 /> */}
      {/* <DateTimeProvider value="2/5/2022">
        <UserProvider value="Tamil">
          <UseContext1 />
        </UserProvider>
      </DateTimeProvider> */}

      {/* <UseRef1 /> */}
      {/* <UseReducer1 /> */}
      <ToDoFunction />

    </div>
  );

}

export default App