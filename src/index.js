import React from 'react';
import ReactDOM from 'react-dom';
import './App.css'
import App from './App'
import PortalAppComponent from './components/PortalComponent';


ReactDOM.render(
  <>
    <App />
    {/* <PortalAppComponent /> */}
  </>
  ,
  document.getElementById('root')
);

